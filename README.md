Gregcsokas
============

Blog and portfolio engine
-------------------------

This an engine of my blog and portfolio site, I&#39;m restart this project about 3 times ... 
hopeless, but I hope I will do it now :D

#### Versioning ####

major.minor.maitenance

(number).(yy.mmdd).(commitnum)

##### examples #####

* ###### 0.17.1019.0 ######
  This is a zeroth major version of the package released in 2017-10-19,
  commited changes are 0.
  
* ###### 0.17.1019.33 ######
  This is again a zeroth major version of the package released the same day as before
  but we have 33 committed changes.
  
##### Installation #####

* make a copy of the *__.env_example__*  and rename to *__.env__*
* edit your *__.env__* file
* Install local requirements with pip
* Edit *__core.json__* in the core fixture folder
* run _manage.py loaddata core.json_ command

Schematic diagram:
------------------
![](docs/source/_static/img/schema.svg "Schematic")