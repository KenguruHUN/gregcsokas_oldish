# -*- coding: utf-8 -*-
from invoke import task

from .helpers import root_folder

root_path = root_folder()


@task
def clean(ctx, bytecode=False, folder=False, exclude='venv'):
    """
    Clean all pycahce folder and bytecode files in the project folder, with exclude option.

    :param ctx: contextualized=true boilerplate of Invoke

    :param bytecode: If true cleaning the `.pyc` files
    :type bytecode: boolean

    :param folder: if true cleaning `__pycache__` folders
    :type folder: boolean

    :param exclude: exclude parameter of clean
    :type exclude: string

    :return: None
    """
    patterns = []

    if bytecode:
        patterns.append('*.pyc')

    if folder:
        patterns.append('__pycache__')

    if len(patterns) != 0:
        for pattern in patterns:
            ctx.run('find {} -name "{}" ! -path "*/{exclude}/*" -delete'.format(root_path, pattern, exclude=exclude))
            print('Deleting \t {} '.format(pattern))
    else:
        print('Nothing to be done')
