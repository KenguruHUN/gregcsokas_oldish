#!/usr/bin/env bash

exec docker run \
     -p 5432:5432 \
     --env POSTGRES_DB=gregcsokas \
     --env POSTGRES_USER=root \
     --env POSTGRES_PASSWORD=root \
     --name postgres_dev \
     postgres:alpine