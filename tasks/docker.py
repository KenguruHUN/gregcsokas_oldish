# -*- coding: utf-8 -*-
from invoke import task


@task
def start(ctx, build=False):
    """
    Start or build docker containers from compose file.

    :param ctx: contextualized=true boilerplate of Invoke

    :param build: Build docker container(s)
    :type build: boolean

    :return: None
    """

    build_param = '--build' if build else ''
    ctx.run('docker-compose up {param}'.format(param=build_param))


@task
def web(ctx):
    """
    Enter the shell of the web container

    :param ctx: contextualized=true boilerplate of Invoke
    :return: None
    """

    ctx.run('docker-compose exec web ash', pty=True)


@task
def postgres(ctx):
    """
    Enter the shell of the database container

    :param ctx: contextualized=true boilerplate of Invoke
    :return: None
    """

    ctx.run('docker-compose exec db psql gregcsokas', pty=True)


@task
def nginx(ctx):
    """
    Enter the shell of the nginx container

    :param ctx: contextualized=true boilerplate of Invoke
    :return: None
    """

    ctx.run('docker-compose exec nginx ash', pty=True)
