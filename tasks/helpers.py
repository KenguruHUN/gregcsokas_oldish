# -*- coding: utf-8 -*-
import os

# valami


def root_folder():
    """
    Return absolute path of the project root directory

    :return: string
    """
    abs_root_path = os.path.dirname(os.path.dirname(os.path.abspath('README.md')))

    return abs_root_path


root_path = root_folder()


def all_requirements():
    """
    Collect all requirements text file name from requirements folder and return as list.

    :return: names
    :rtype: list
    """

    names = []

    requirements_files = os.listdir(os.path.join(root_path, 'requirements/'))
    for file in requirements_files:
        file_name = file.split('.')[0]
        names.append(file_name)

    return names
