# -*- coding: utf-8 -*-
from invoke import task

from .helpers import all_requirements


@task
def env(ctx, requirement='local'):
    """
    Initialization of the environment (production or development)

    :param ctx: contextualized=true boilerplate of Invoke

    :param requirement: type of environment eg. local, production, etc.
    :type requirement: string

    :return: none
    """

    possible_names = all_requirements()
    if requirement in possible_names:
        ctx.run('pip install -r requirements/{0}.txt'.format(requirement))
    else:
        print('Nincs ilyen requirements file!')
