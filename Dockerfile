FROM alpine:latest

LABEL maintainer = "Csókás Gergő <gergo.csokas@gmail.com>"

ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /usr/bin/python3
ENV PATH /opt/gregcsokas/src:$PATH
ENV DJANGO_SETTINGS_MODULE gregcsokas.settings.development

VOLUME /opt/gregcsokas
VOLUME /opt/sockets
VOLUME /srv/gregcsokas

RUN apk update && apk upgrade --no-cache

RUN apk add --no-cache \
            python3 \
            zlib \
            postgresql-dev \
            libffi \
            jpeg && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    rm -r /root/.cache && \
    apk add --no-cache --virtual .build-deps \
            alpine-sdk \
            python3-dev \
            zlib-dev \
            libffi-dev \
            jpeg-dev

COPY requirements /opt/gregcsokas/requirements
RUN python3 -m pip install --no-cache-dir -r /opt/gregcsokas/requirements/local.txt

RUN apk del .build-deps

COPY .env_example /opt/gregcsokas/.env
COPY config/docker-entrypoint-development.sh /opt/gregcsokas/docker-entrypoint.sh

COPY config/invoke.yaml /etc

COPY tasks /opt/gregcsokas/tasks
COPY src /opt/gregcsokas/src

COPY config/gunicorn /etc/gunicorn/conf

WORKDIR /opt/gregcsokas/src

CMD sh /opt/gregcsokas/docker-entrypoint.sh
