# -*- coding: utf-8 -*-
from django.db import models
from django.contrib import admin
from martor.widgets import AdminMartorWidget

from core.models import MetaData, SinglePage


class SinglePageModelAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminMartorWidget},
    }


admin.site.register(MetaData)
admin.site.register(SinglePage, SinglePageModelAdmin)
