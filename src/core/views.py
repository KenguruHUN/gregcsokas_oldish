# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from django.views import generic

from .models import SinglePage


class SinglePageView(generic.TemplateView):
    template_name = 'core/simple_view.html'

    def get_context_data(self, **kwargs):
        context = super(SinglePageView, self).get_context_data(**kwargs)
        context['content'] = get_object_or_404(SinglePage, filter_url=self.request.path)

        return context
