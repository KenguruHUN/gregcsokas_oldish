# -*- coding: utf-8 -*-
from django.db.models import Q

from . import models as core_m


def page_specific_meta(request):
    context = dict()
    queryset = core_m.MetaData.objects.filter(Q(filter_url=request.path) | Q(filter_url=''))
    context['meta'] = queryset.first()

    if len(context['meta'].keywords) != 0:
        context['meta'].keywords = ', '.join(str(k) for k in context['meta'].keywords)

    return context
