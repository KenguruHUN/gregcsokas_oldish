# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils.translation import ugettext_lazy as _

from martor.models import MartorField


class FilterUrl(models.Model):
    filter_url = models.CharField(_('text type url field for metadata'),
                                  max_length=100, unique=True, blank=True, null=True)

    class Meta:
        abstract = True


class MetaData(FilterUrl):
    title = models.CharField(_('page title'), max_length=60)
    viewport = models.CharField(_('viewport'), max_length=100, default='width=device-width, initial-scale=1')  # noqa
    description = models.CharField(_('page description'), max_length=165)
    keywords = ArrayField(models.CharField(max_length=10, blank=True), size=8)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, limit_choices_to={'is_staff': True})  # noqa

    class Meta:
        pass

    def __str__(self):
        return self.title


class SinglePage(FilterUrl):
    title = models.CharField(_('page title'), max_length=60)
    content = MartorField(_('page content'))

    class Meta:
        pass

    def __str__(self):
        return self.title
