# -*- coding: utf-8 -*-
import pytest

from blog.models import Category, Tag

pytestmark = pytest.mark.django_db


class TestCreateCategories:

    def test_simple_category(self, category_1):
        category = Category.objects.first()

        assert category.name == "Test Category 1"
        assert category.slug == "test-category-1"

    def test_simple_category_with_accents(self, category_2_with_accents):
        category = Category.objects.first()

        assert category.name == "Teszt Kategória 2"
        assert category.slug == "teszt-kategoria-2"


class TestCreateTag:

    def test_simple_tag(self, tag_1):
        tag = Tag.objects.first()

        assert tag.name == 'test'

    def test_tag_with_accents(self, tag_2):
        tag = Tag.objects.first()

        assert tag.name == 'ékezetes'

    def test_multiple_tag(self, tag_1, tag_2, tag_3):
        tag = Tag.objects.all()

        assert tag[0].name == 'ékezetes'
        assert tag[1].name == 'test2'
        assert tag[2].name == 'test'


# class TestCreatePost:
#
#     def test_create_simple_post(self, post_1):
#         pass
