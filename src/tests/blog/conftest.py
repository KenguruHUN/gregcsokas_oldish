# -*- coding: utf-8 -*-
import pytest

from blog.models import Category, Tag, Post


@pytest.fixture
def category_1():
    return Category.objects.create(name='Test Category 1')


@pytest.fixture
def category_2_with_accents():
    return Category.objects.create(name='Teszt Kategória 2')


@pytest.fixture
def tag_1():
    return Tag.objects.create(name='test')


@pytest.fixture
def tag_2():
    return Tag.objects.create(name='ékezetes')


@pytest.fixture
def tag_3():
    return Tag.objects.create(name='test2')


# @pytest.fixture
# def post_1():
#     return Post.objects.create()
