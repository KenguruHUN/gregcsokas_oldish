# -*- coding: utf-8 -*-
import pytest
from django.test import RequestFactory, Client
from core.models import MetaData


@pytest.fixture
def rf():
    return RequestFactory()


@pytest.fixture
def cl():
    return Client()


@pytest.fixture
def default_meta():
    return MetaData.objects.create(filter_url="",
                                   title="Test title",
                                   description="Test description",
                                   keywords="{test, keywords, pytest}")


@pytest.fixture
def index_meta():
    return MetaData.objects.create(filter_url="/",
                                   title="Index title",
                                   description="Index description",
                                   keywords="{index, test, keywords, pytest}")


@pytest.fixture
def about_meta():
    return MetaData.objects.create(filter_url="/about/",
                                   title="About title",
                                   description="About description",
                                   keywords="{about, test, keywords, pytest}")
