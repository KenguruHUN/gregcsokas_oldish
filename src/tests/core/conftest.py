# -*- coding: utf-8 -*-
import pytest

from core.models import SinglePage


@pytest.fixture
def default_index_page():
    return SinglePage.objects.create(filter_url="/",
                                     title="Default index page",
                                     content="# Title \n Test content")


@pytest.fixture
def default_about_page():
    return SinglePage.objects.create(filter_url="/about/",
                                     title="Default about page",
                                     content="# AboutTest \n About content")
