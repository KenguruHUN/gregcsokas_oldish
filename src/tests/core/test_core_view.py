# -*- coding: utf-8 -*-
import pytest

from tests.helpers import setup_view
from core.models import MetaData
from core.views import SinglePageView


pytestmark = pytest.mark.django_db


class TestSimplePageView:

    def test__index__page_with__default_meta__(self, rf, default_meta, default_index_page):
        request = rf.get('/')
        view = SinglePageView()
        view = setup_view(view, request)
        context = view.get_context_data()

        rendered = view.render_to_response(context)

        assert rendered.rendered_content.__contains__('<title>Test title</title>')
        assert rendered.rendered_content.__contains__('<meta name="description" content="Test description" />')  # noqa
        assert rendered.rendered_content.__contains__('<meta name="keywords" content="test, keywords, pytest" />')  # noqa
        assert rendered.rendered_content.__contains__("<h1>Title</h1>\n<p>Test content</p>")
        assert context['content'].title == 'Default index page'

    def test__index__page_with__page_specific_meta__(self, rf, index_meta, default_index_page):
        request = rf.get('/')
        view = SinglePageView()
        view = setup_view(view, request)
        context = view.get_context_data()

        rendered = view.render_to_response(context)

        assert rendered.rendered_content.__contains__('<title>Index title</title>')
        assert rendered.rendered_content.__contains__('<meta name="description" content="Index description" />')  # noqa
        assert rendered.rendered_content.__contains__('<meta name="keywords" content="index, test, keywords, pytest" />')  # noqa
        assert rendered.rendered_content.__contains__("<h1>Title</h1>\n<p>Test content</p>")
        assert context['content'].title == 'Default index page'

    def test__about__page_with__default_meta__(self, rf, default_meta, default_about_page):
        request = rf.get('/about/')
        view = SinglePageView()
        view = setup_view(view, request)
        context = view.get_context_data()

        rendered = view.render_to_response(context)

        assert rendered.rendered_content.__contains__('<title>Test title</title>')
        assert rendered.rendered_content.__contains__('<meta name="description" content="Test description" />')  # noqa
        assert rendered.rendered_content.__contains__('<meta name="keywords" content="test, keywords, pytest" />')  # noqa
        assert rendered.rendered_content.__contains__("<h1>AboutTest</h1>\n<p>About content</p>")
        assert context['content'].title == 'Default about page'

    def test__about__page_with__page_specific_meta__(self, rf, about_meta, default_about_page):
        request = rf.get('/about/')
        view = SinglePageView()
        view = setup_view(view, request)
        context = view.get_context_data()

        rendered = view.render_to_response(context)

        assert rendered.rendered_content.__contains__('<title>About title</title>')
        assert rendered.rendered_content.__contains__('<meta name="description" content="About description" />')  # noqa
        assert rendered.rendered_content.__contains__('<meta name="keywords" content="about, test, keywords, pytest" />')  # noqa
        assert rendered.rendered_content.__contains__("<h1>AboutTest</h1>\n<p>About content</p>")
        assert context['content'].title == 'Default about page'
