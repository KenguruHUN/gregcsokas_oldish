# -*- coding: utf-8 -*-
from .models import Category

def blog_category_links(request):
    context = dict()
    context['categories'] = Category.objects.all()
    # context['categories'] = [category for category in queryset]

    return context
