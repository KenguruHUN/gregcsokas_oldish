# -*- coding: utf-8 -*-
from django.urls import path

from . import views

app_name = 'blog'
urlpatterns = [
    path('', views.BlogIndexView.as_view(), name='index'),
    path('<category>/', views.PostByCategoryListView.as_view(), name='category'),
    path('<category>/<slug:slug>', views.PostDetailView.as_view(), name='detail'),
]
