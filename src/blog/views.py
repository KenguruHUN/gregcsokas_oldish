# -*- coding: utf-8 -*-
import logging

from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.views import generic
from django.utils import timezone

from blog.models import Post


logger = logging.getLogger('grecsokas' + __name__)


class BlogIndexView(generic.ListView):
    template_name = 'blog/post_list.html'
    queryset = Post.objects.all()

    def get_queryset(self, **kwargs):
        queryset = super(BlogIndexView, self).get_queryset().prefetch_related('category').prefetch_related('author')
        if not self.request.user.is_staff:
            queryset = queryset.exclude(pub_date__gte=timezone.now()) \
                .exclude(status='DR') \
                .exclude(status='PR')

        return queryset


class PostByCategoryListView(generic.ListView):
    template_name = 'blog/post_list.html'
    queryset = Post.objects.all().prefetch_related('category').prefetch_related('author')

    def get_queryset(self, **kwargs):
        queryset = super(PostByCategoryListView, self).get_queryset()\
            .filter(category__name__iexact=self.kwargs['category'])
        if not self.request.user.is_staff:
            queryset = queryset.exclude(pub_date__gte=timezone.now()) \
                .exclude(status='DR') \
                .exclude(status='PR')

        return queryset


class PostDetailView(generic.DetailView):
    template_name = 'blog/post_detail.html'
    queryset = Post.objects.all()

    def get_queryset(self, **kwargs):
        queryset = super(PostDetailView, self).get_queryset()\
            .prefetch_related('category').prefetch_related('author') \
            .filter(slug=self.kwargs['slug'])
        if not self.request.user.is_staff:
            queryset = queryset.exclude(pub_date__gte=timezone.now()) \
                .exclude(status='DR') \
                .exclude(status='PR')

        return queryset

    def get_object(self, queryset=None):
        queryset = self.get_queryset()

        try:
            obj = queryset.get()
        except ObjectDoesNotExist:
            raise Http404("No post matches the given query.")

        return obj
